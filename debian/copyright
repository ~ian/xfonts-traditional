The machinery in this package is Copyright (C) 2012,2016 Ian Jackson.
It is released under the following licence:

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
  SOFTWARE IN THE PUBLIC INTEREST, INC.  BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
  DEALINGS IN THE SOFTWARE.

Most font glyph shape information is from pcfs which were copied out of
xfonts-base.  For "good", version 3.3.6-2 and for "bad" 1:1.0.1.

bad/10x20.pcf.gz and bad/9x15.pcf.gz came from xfonts-base
1:1.0.4+nmu1.  (In the latter case, from the file
9x15-ISO8859-1.pcf.gz since it is smaller and contains the same
glyphs.)

bad/neep-alt-iso8859-1-*.pcf.gz are from xfonts-jmk 3.0-20.

Most pcfs were originally generated from source code in that
xfonts-base; but here, in xfonts-traditional, we do not modify them
any longer.  (It would be possible to build-depend on the relevant
xfonts-base, but this would make life worse rather than better because
it would make the resulting xfonts-traditional binary package
nonfunctional if you happened to already have a traditional xfonts-base
installed on the build host.)

The font 6x13O is an exception: no oblique version of 6x13 existed in
xfonts-base 3.3.6-2.  Mark Wooding created the `good' version of this font by
hand-editing the `bad' version.  The edited version, like the original, is in
the public domain.

good/neep-alt-iso8859-1-*.sfd and good/10x20.sfd were modified from
the corresponding bad/*.pcf.gz by Ian Jackson using fontforge and are
under the same licence as the .pcf.gz.

The copyright files for the font packages follow.

======================================================================
======================================================================
[ xfonts-base 3.3.6-2 copyright information: ]

This package is part of the Debian pre-packaged version of XFree86
release 3.3.6.

Version 3.3.6 was assembled by Branden Robinson <branden@debian.org> from:
ftp://ftp.xfree86.org/pub/xfree86/3.3.6/source/X336src-2.tgz

Unless otherwise noted, all modifications and additions to XFree86 found in
Debian packages bear the following copyright:

Copyright 1996-2000 Software in the Public Interest, Inc.

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
SOFTWARE IN THE PUBLIC INTEREST, INC.  BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of Software in the Public
Interest, Inc. shall not be used in advertising or otherwise to promote the
sale, use or other dealings in this Software without prior written
authorization from Software in the Public Interest, Inc.

********************************************************************************

Copyright (C) 1996 X Consortium

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of the X Consortium shall
not be used in advertising or otherwise to promote the sale, use or
other dealings in this Software without prior written authorization
from the X Consortium.

X Window System is a trademark of X Consortium, Inc.

======================================================================
======================================================================
[ xfonts-base 1:1.0.1 copyright information: ]

Source package: xfonts-base
Obtained from:
    http://xorg.freedesktop.org/releases/individual/font/
        font-arabic-misc-X11R7.0-1.0.0
        font-cursor-misc-X11R7.0-1.0.0
        font-daewoo-misc-X11R7.0-1.0.0
        font-dec-misc-X11R7.0-1.0.0
        font-isas-misc-X11R7.0-1.0.0
        font-jis-misc-X11R7.0-1.0.0
        font-micro-misc-X11R7.0-1.0.0
        font-misc-misc-X11R7.0-1.0.0
        font-mutt-misc-X11R7.0-1.0.0
        font-schumacher-misc-X11R7.0-1.0.0
        font-sony-misc-X11R7.0-1.0.0
        font-sun-misc-X11R7.0-1.0.0
Upstream author(s):
    various; see copyright notices below
Debian package author(s):
    Stephen Early
    Mark Eichin
    Branden Robinson
    ISHIKAWA Mutsumi
    Scott James Remnant
    Daniel Stone
    David Nusinow

Debian copyright(s)/licence(s):

Unless otherwise noted, all modifications and additions to the upstream sources
found in this Debian package bear the following copyright and license terms:

    Copyright 1996-2004 Branden Robinson

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHOR(S) AND COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.

Upstream copyright(s)/licence(s):

font-arabic-misc-X11R7.0-1.0.0/COPYING:
    Copyright 1996, 1997, 1998, 1999 Computing Research Labs, New Mexico State
    University

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    COMPUTING RESEARCH LAB OR NEW MEXICO STATE UNIVERSITY BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    USE OR OTHER DEALINGS IN THE SOFTWARE.

font-cursor-misc-X11R7.0-1.0.0/COPYING:
    "These ""glyphs"" are unencumbered"

font-daewoo-misc-X11R7.0-1.0.0/COPYING:
    "Copyright (c) 1987, 1988 Daewoo Electronics Co.,Ltd."

font-dec-misc-X11R7.0-1.0.0/COPYING:
    "Copyright (c) Digital Equipment Corporation,1988. All rights reserved"

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted, provided
    that the above copyright notices appear in all copies and that both those
    copyright notices and this permission notice appear in supporting
    documentation, and that the name Digital Equipment Corporation not be used
    in advertising or publicity pertaining to distribution of the software
    without specific, written prior permission.  Digital Equipment Corporation
    makes no representations about the suitability of this software for any
    purpose.  It is provided "as is" without express or implied warranty.

    DIGITAL EQUIPMENT CORPORATION DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
    SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
    IN NO EVENT SHALL DIGITAL EQUIPMENT CORPORATION BE LIABLE FOR ANY SPECIAL,
    INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
    OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

font-isas-misc-X11R7.0-1.0.0/COPYING:
    Copyright (C) 1988  The Institute of Software, Academia Sinica.

    Correspondence Address:  P.O.Box 8718, Beijing, China 100080.

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted, provided
    that the above copyright notices appear in all copies and that both those
    copyright notices and this permission notice appear in supporting
    documentation, and that the name of "the Institute of Software, Academia
    Sinica" not be used in advertising or publicity pertaining to distribution
    of the software without specific, written prior permission.  The Institute
    of Software, Academia Sinica, makes no representations about the suitability
    of this software for any purpose.  It is provided "as is" without express or
    implied warranty.

    THE INSTITUTE OF SOFTWARE, ACADEMIA SINICA, DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS, IN NO EVENT SHALL THE INSTITUTE OF SOFTWARE, ACADEMIA SINICA,
    BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
    OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
    CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

font-jis-misc-X11R7.0-1.0.0/COPYING:
    from Japanese Industrial Standard, JIS X 9051-1984
    named "16-dots Matrix Character Patterns for Display Devices"
    "from JIS X 9051-1984, by permission to use"

font-micro-misc-X11R7.0-1.0.0/COPYING:
    "Public domain font.  Share and enjoy."

font-misc-misc-X11R7.0-1.0.0/COPYING:
    "Public domain font.  Share and enjoy."

font-mutt-misc-X11R7.0-1.0.0/COPYING:
    Copyright 2002 Computing Research Labs, New Mexico State University

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    COMPUTING RESEARCH LAB OR NEW MEXICO STATE UNIVERSITY BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    USE OR OTHER DEALINGS IN THE SOFTWARE.

font-schumacher-misc-X11R7.0-1.0.0/COPYING:
    Copyright 1989 Dale Schumacher, dal@syntel.mn.org
                   399 Beacon Ave.
                   St. Paul, MN  55104-3527

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted, provided
    that the above copyright notice appear in all copies and that both that
    copyright notice and this permission notice appear in supporting
    documentation, and that the name of Dale Schumacher not be used in
    advertising or publicity pertaining to distribution of the software without
    specific, written prior permission.  Dale Schumacher makes no
    representations about the suitability of this software for any purpose.  It
    is provided "as is" without express or implied warranty.

font-sony-misc-X11R7.0-1.0.0/COPYING:
    Copyright 1989 by Sony Corp.

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted, provided
    that the above copyright notices appear in all copies and that both those
    copyright notices and this permission notice appear in supporting
    documentation, and that the name of Sony Corp.  not be used in advertising
    or publicity pertaining to distribution of the software without specific,
    written prior permission.  Sony Corp. makes no representations about the
    suitability of this software for any purpose.  It is provided "as is"
    without express or implied warranty.

    SONY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL SONY BE
    LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
    OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
    CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

font-sun-misc-X11R7.0-1.0.0/COPYING:
    Copyright 1989 by Sun Microsystems, Inc. Mountain View, CA.

    Permission to use, copy, modify, and distribute this software and
    its documentation for any purpose and without fee is hereby granted,
    provided that the above copyright notices appear in all copies and
    that both those copyright notices and this permission notice appear
    in supporting documentation, and that the name of Sun Microsystems
    not be used in advertising or publicity pertaining to distribution
    of the software without specific, written prior permission.  Sun
    Microsystems make no representations about the suitability of this
    software for any purpose.  It is provided "as is" without express or
    implied warranty.

    SUN MICROSYSTEMS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
    SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
    FITNESS, IN NO EVENT SHALL SUN MICROSYSTEMS BE LIABLE FOR ANY
    SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
    RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
    CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
    CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

vim:set ai et sw=4 ts=4 tw=80:

======================================================================
======================================================================
[ xfonts-base 1:1.0.4+nmu1 copyright information: ]

Source package: xfonts-base
Obtained from:
    http://xorg.freedesktop.org/releases/individual/font/
        font-arabic-misc
        font-cursor-misc
        font-daewoo-misc
        font-dec-misc
        font-isas-misc
        font-jis-misc
        font-micro-misc
        font-misc-misc
        font-mutt-misc
        font-schumacher-misc
        font-sony-misc
        font-sun-misc
Upstream author(s):
    various; see copyright notices below
Debian package author(s):
    Stephen Early
    Mark Eichin
    Branden Robinson
    ISHIKAWA Mutsumi
    Scott James Remnant
    Daniel Stone
    David Nusinow

Debian copyright(s)/licence(s):

Unless otherwise noted, all modifications and additions to the upstream sources
found in this Debian package bear the following copyright and license terms:

    Copyright 1996-2004 Branden Robinson

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    AUTHOR(S) AND COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.

Upstream copyright(s)/licence(s):

font-arabic-misc/COPYING:
    Copyright 1996, 1997, 1998, 1999 Computing Research Labs, New Mexico State
    University

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    COMPUTING RESEARCH LAB OR NEW MEXICO STATE UNIVERSITY BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    USE OR OTHER DEALINGS IN THE SOFTWARE.

font-cursor-misc/COPYING:
    "These ""glyphs"" are unencumbered"

font-daewoo-misc/COPYING:
    "Copyright (c) 1987, 1988 Daewoo Electronics Co.,Ltd."

font-dec-misc/COPYING:
    "Copyright (c) Digital Equipment Corporation,1988. All rights reserved"

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted, provided
    that the above copyright notices appear in all copies and that both those
    copyright notices and this permission notice appear in supporting
    documentation, and that the name Digital Equipment Corporation not be used
    in advertising or publicity pertaining to distribution of the software
    without specific, written prior permission.  Digital Equipment Corporation
    makes no representations about the suitability of this software for any
    purpose.  It is provided "as is" without express or implied warranty.

    DIGITAL EQUIPMENT CORPORATION DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
    SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
    IN NO EVENT SHALL DIGITAL EQUIPMENT CORPORATION BE LIABLE FOR ANY SPECIAL,
    INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
    OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

font-isas-misc/COPYING:
    Copyright (C) 1988  The Institute of Software, Academia Sinica.

    Correspondence Address:  P.O.Box 8718, Beijing, China 100080.

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted, provided
    that the above copyright notices appear in all copies and that both those
    copyright notices and this permission notice appear in supporting
    documentation, and that the name of "the Institute of Software, Academia
    Sinica" not be used in advertising or publicity pertaining to distribution
    of the software without specific, written prior permission.  The Institute
    of Software, Academia Sinica, makes no representations about the suitability
    of this software for any purpose.  It is provided "as is" without express or
    implied warranty.

    THE INSTITUTE OF SOFTWARE, ACADEMIA SINICA, DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS, IN NO EVENT SHALL THE INSTITUTE OF SOFTWARE, ACADEMIA SINICA,
    BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
    OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
    CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

font-jis-misc/COPYING:
    from Japanese Industrial Standard, JIS X 9051-1984
    named "16-dots Matrix Character Patterns for Display Devices"
    "from JIS X 9051-1984, by permission to use"

font-micro-misc/COPYING:
    "Public domain font.  Share and enjoy."

font-misc-misc/COPYING:
    "Public domain font.  Share and enjoy."

font-mutt-misc/COPYING:
    Copyright 2002 Computing Research Labs, New Mexico State University

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
    COMPUTING RESEARCH LAB OR NEW MEXICO STATE UNIVERSITY BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    USE OR OTHER DEALINGS IN THE SOFTWARE.

font-schumacher-misc/COPYING:
    Copyright 1989 Dale Schumacher, dal@syntel.mn.org
                   399 Beacon Ave.
                   St. Paul, MN  55104-3527

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted, provided
    that the above copyright notice appear in all copies and that both that
    copyright notice and this permission notice appear in supporting
    documentation, and that the name of Dale Schumacher not be used in
    advertising or publicity pertaining to distribution of the software without
    specific, written prior permission.  Dale Schumacher makes no
    representations about the suitability of this software for any purpose.  It
    is provided "as is" without express or implied warranty.

font-sony-misc/COPYING:
    Copyright 1989 by Sony Corp.

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted, provided
    that the above copyright notices appear in all copies and that both those
    copyright notices and this permission notice appear in supporting
    documentation, and that the name of Sony Corp.  not be used in advertising
    or publicity pertaining to distribution of the software without specific,
    written prior permission.  Sony Corp. makes no representations about the
    suitability of this software for any purpose.  It is provided "as is"
    without express or implied warranty.

    SONY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL SONY BE
    LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
    OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
    CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

font-sun-misc/COPYING:
    Copyright (c) 1989, Oracle and/or its affiliates. All rights reserved.

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice (including the next
    paragraph) shall be included in all copies or substantial portions of the
    Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.

vim:set ai et sw=4 ts=4 tw=80:

======================================================================
======================================================================
[ xfonts-jmk 3.0-20 copyright information: ]

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: http://www.jmknoble.net/fonts/
Upstream-Contact: Jim Knoble <jmknoble@pobox.com>
Upstream-Name: Jim's Fonts for X

Files: *
Copyright: 1998 Jim Knoble <jmknoble@pobox.com>
License: GPL-2+
Comment: Several additional patches and other contributions, including the
 ISO-10646-1 10x20 Neep font, were provided by Derek Upham.  Derek Upham
 has placed his modifications in the public domain.
 .
 The glyphs for the ISO-10646-1 10x20 Neep font were taken from the
 misc-fixed font maintained by Marcus Kuhn.  Those glyphs have the
 following copyright notice:
 .
 Public domain font.  Share and enjoy.
 .
 Since these changes are all compatible with the base package license of
 GPL v2 or later, it's easiest to think of the entire contents of the
 package as covered by that license.

Files: debian/*
Copyright: 1999, 2001 Joseph Carter <knghtbrd@debian.org>
  2004-2008, 2010, 2013 Russ Allbery <rra@debian.org>
License: GPL-2+
Comment: Changes by Joseph Carter did not have explicit copyright or
 license statements.  Presumbly those changes may be redistributed under
 the same terms as the fonts in this package.
 .
 All changes and Debian packaging files by Russ Allbery are hereby placed
 in the public domain.  If this is not possible in your jurisdiction,
 please consider this a license to treat these files in all respects as if
 they were in the public domain.

License: GPL-2+
 I have chosen to release them under the terms of the GNU General Public
 License (GPL) version 2.0, or, at your option, any later version.  This
 means that you may freely modify and distribute these fonts, as long as
 you distribute the source code for the fonts as well.
 .
 Some few parts of these fonts are derived from the 6x13 fixed font
 accompanying the XFree86-3.3.2 distribution of the X Window System.  That
 font bears the notice "Public domain font; share and enjoy".
 .
 The vast majority of this set of fonts is original work.  Besides the
 derivations noted above, any resemblance to other fonts, copyrighted or
 not, may be accounted for by standard letter shapes and the relatively
 coarse resolution (75dpi) for which the fonts were created.
 .
 No warranty of any kind, either express or implied, including that of
 merchantibility or fitness for a particular purpose.  If anything breaks,
 you get to keep both pieces.  Your mileage may vary.  Eat your
 vegetables.
Comment: You can find the GPL version 2 in /usr/share/common-licenses/GPL-2
 on Debian systems.
