SHELL=/bin/bash

prefix?=/usr/local

package=xfonts-traditional
utility=update-$(package)

install=install -o 0 -g 0

default: all

OWNFONT = 6x13O
SRCBDF = $(patsubst %, bdfsrc/%.bdf, $(OWNFONT))
GENPCF = $(patsubst %, good/%.pcf.gz, $(OWNFONT))
$(GENPCF): good/%.pcf.gz: bdfsrc/%.bdf
	bdftopcf <$< | gzip -9 >$@

all: $(GENPCF)
	mkdir -p rules/
	./mkrules <specs
	cp manual/*.rules rules/

install:
	perl -pe 's#^(our \$$prefix=").*("\;\s*)$$#$$1$(prefix)$$2#' \
		<$(utility) >$(utility).sedded
	$(install) -m755 -d $(DESTDIR)$(prefix)/{bin,share/{man/man8,$(package)/rules}}
	$(install) -m755 $(utility).sedded $(DESTDIR)$(prefix)/bin/$(utility)
	$(install) -m755 $(utility).8 $(DESTDIR)$(prefix)/share/man/man8/.
	$(install) -m644 rules/* $(DESTDIR)$(prefix)/share/$(package)/rules/
	$(install) -m755 bdfnorm $(DESTDIR)$(prefix)/share/$(package)/

clean:
	rm -f rules/*.rules rules/foundries rules/foundries.new
	rm -f good/*.bdf bad/*.bdf
	rm -f $(GENPCF)
	rm -f *~ $(utility).sedded
